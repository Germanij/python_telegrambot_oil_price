from src.chatbot.bot import telegram_chatbot
from src.Experiment_Oil_Price import PriceExtractor

update_id = None
bot = telegram_chatbot('config.cfg')

def get_data():
    link = 'https://www.esyoil.com/?calc%5Bzipcode%5D=89547&calc%5Bamount%5D=3000&calc%5Bunloading_points%5D=1&calc%5Bprod%5D=8&calc%5Bpayment_type%5D=2&calc%5Bexpress%5D=0&calc%5Bdelivery_date_sel%5D=&calc%5Bhose%5D=40m&calc%5Bshort_vehicle%5D=&calc%5Bsubmit%5D='
    data = PriceExtractor(link)
    oilprice = 'Hallo! Der momentan beste Heizölpreis liegt bei: ' + str(data.get_price()) + '€/100Liter' + '\n' + data.get_check_for_limit()
    return oilprice


def make_reply(msg):
    if msg is not None:
        reply = get_data()
    return reply


while True:
    print('...')
    updates = bot.get_updates(offset=update_id)
    print(updates)
    if updates.get('result'):
        for item in updates.get('result'):
            update_id = item['update_id']
            try:
                message = item['message']['text']
            except:
                message = None
            from_ = item['message']['from']['id']
            reply = make_reply(message)
            bot.send_message(reply, from_)

from bs4 import BeautifulSoup
import requests
from datetime import datetime

class PriceExtractor:
    def __init__(self, site_url):
        self.site_url = site_url
        self._soup = None
        self.oil_price = None
        self.place_time = None
        self.wanted_price = int(68)
        self.link = None

        try:
            source = requests.get(self.site_url).text
            self._soup = BeautifulSoup(source, 'html.parser')
        except:
            pass


    def get_price(self):
        try:
            oil_value = self._soup.find('data', class_='price').text
            oil_value = oil_value.replace(',','.')
            self.oil_price = float(oil_value)
            return self.oil_price
        except:
            message = 'Ölpreis konnte nicht gefunden werden'
            return message


    def get_time_place(self):
        try:
            self.place_time = self._soup.find('span', {'id': 'h4-until-base-3'}).text
            return self.place_time
        except:
            message = 'Ort und Zeit konnten nicht gefunden werden'
            return message
    

    def get_link(self):
        try:
            pass
        except:
            pass

    def get_check_for_limit(self):
        try:
            self.get_price()
            if self.wanted_price > self.oil_price:
                message = 'Der Ölpreis ist unter dem von dir festgelegten Wunschpreis! Folge dem Link https://bit.ly/2M1UqNX um direkt zu bestellen!'
                return message
            else:
                message = 'Der Preis ist zu hoch'
                return message
        except:
            return None


#print(PriceExtractor(link1).get_check_for_limit())


